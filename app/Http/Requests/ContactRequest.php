<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'infor' => 'required|max:128|min:2|unique:contacts,infor',
        ];
    }

    public function messages()
    {
        return [
            'infor.required' => "Infor is a required field",
            'infor.min' => "Infor must contain at least 2 characters",
            'infor.max' => "Infor must contain at most 128 characters.....",
            'infor.unique' => "Infor already exist",
        ];
    }

}
