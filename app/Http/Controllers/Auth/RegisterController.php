<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,
            [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ],
            [
                'name.required' => 'Name is a required field',
                'name.max' => 'Name must contain at most 255 characters',
                'email.required' => 'Email is a required field',
                'email.email' => 'Invalid email',
                'email.max' => 'Email must contain at most 255 characters',
                'email.unique' => 'Email already exists',
                'password.required' => 'Password is a required field',
                'password.min' => 'Password must contain at least 8 characters',
                'password.confirmed' => 'Invalid password',
            ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    public function getRegister()
    {
        return view('auth/register');
    }

    public function postRegister(Request $request)
    {
        // Check input
        $allRequest = $request->all();
        $validator = $this->validator($allRequest);

        if ($validator->fails()) {
            // Incomplete input data -> notification error
            return redirect('register')->withErrors($validator)->withInput();
        } else {
            // Complete input data -> create user in Database
            if ($this->create($allRequest)) {
                // Insert success -> notification success
                Session::flash('success', 'Register success!');
                return redirect('login');
            } else {
                // Insert fail -> notification error
                Session::flash('error', 'Register failed!');
                return redirect('register');
            }
        }
    }

}
