// AJAX CRUD operations
// add a new post
$(document).on('click', '.add-modal', function () {
    $('#id_add').val('');
    $('#factory_add').val('');
    $('#job_add').val('');
    $('#describe_add').val('');
    $('#addModal').modal('show');
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('.modal-footer').on('click', '.add', function () {
    $.ajax({
        type: 'POST',
        url: 'posts',
        data: {
            'factory': $('#factory_add').val(),
            'job': $('#job_add').val(),
            'describe': $('#describe_add').val(),
            'contact_id': $('#contact_add').val(),
        },
        success: function (data) {
            toastr.success('Successfully added Post!', 'Success Alert', {timeOut: 5000});
            $('#postTable').append("<tr class='item" + data.posts.id + "'>"
                + "<td>" + data.posts.id + "</td>"
                + "<td>" + data.posts.factory + "</td>"
                + "<td>" + data.posts.job + "</td>"
                + "<td>" + data.posts.describe + "</td>"
                + "<td><a href=\"posts/contact/" + data.posts.contact_id + "\">" + data.contacts + "</a></td>"
                + "<td>Right now</td>"
                + "<td>"
                + "<button class='edit-modal btn btn-info mr-1' data-id='" + data.posts.id + "' data-factory='" + data.posts.factory + "' data-job='" + data.posts.job + "' data-describe='" + data.posts.describe + "' data-contact_id='" + data.posts.contact_id + "'><span class='glyphicon glyphicon-edit'></span> Edit</button>"
                + "<button class='delete-modal btn btn-danger' data-id='" + data.posts.id + "' data-factory='" + data.posts.factory + "' data-job='" + data.posts.job + "' data-describe='" + data.posts.describe + "' data-contact_id='" + data.posts.contact_id + "'><span class='glyphicon glyphicon-trash'></span> Delete</button>"
                + "</td>"
                + "</tr>");
        },
        error: function (data) {
            res = $.parseJSON(data.responseText);
            $.each(res.errors, function (index, value) {
                toastr.error(value);
            });
        }
    });
});

// Edit a post
$(document).on('click', '.edit-modal', function () {
    $('.modal-title').text('Edit');
    //take data
    $('#id_edit').val($(this).data('id'));
    $('#factory_edit').val($(this).data('factory'));
    $('#job_edit').val($(this).data('job'));
    $('#contact_edit').val($(this).data('contact_id'));
    $('#describe_edit').val($(this).data('describe'));
    id = $(this).data('id');
    //show modal
    $('#editModal').modal('show');
});
$('.modal-footer').on('click', '.edit', function () {
    $.ajax({
        type: 'PUT',
        url: 'posts/' + id,
        data: {
            'id': $("#id_edit").val(),
            'factory': $('#factory_edit').val(),
            'job': $('#job_edit').val(),
            'contact_id': $('#contact_edit').val(),
            'describe': $('#describe_edit').val(),
        },
        success: function (data) {
            toastr.success('Successfully updated Post!', 'Success Alert', {timeOut: 5000});
            $('.item' + data.posts.id).replaceWith("<tr class='item" + data.posts.id + "'>"
                + "<td>" + data.posts.id + "</td>"
                + "<td>" + data.posts.factory + "</td>"
                + "<td>" + data.posts.job + "</td>"
                + "<td>" + data.posts.describe + "</td>"
                + "<td><a href=\"posts/contact/" + data.posts.contact_id + "\">" + data.contacts + "</a></td>"
                + "<td>Right now</td>"
                + "<td>"
                + "<button class='edit-modal btn btn-info mr-1' data-id='" + data.posts.id + "' data-factory='" + data.posts.factory + "' data-job='" + data.posts.job + "' data-describe='" + data.posts.describe + "' data-contact_id='" + data.posts.contact_id + "'><span class='glyphicon glyphicon-edit'></span> Edit</button>"
                + "<button class='delete-modal btn btn-danger' data-id='" + data.posts.id + "' data-factory='" + data.posts.factory + "' data-job='" + data.posts.job + "' data-describe='" + data.posts.describe + "' data-contact_id='" + data.posts.contact_id + "'><span class='glyphicon glyphicon-trash'></span> Delete</button>"
                + "</td>"
                + "</tr>"
            );
        },
        error: function (data) {
            res = $.parseJSON(data.responseText);
            $.each(res.errors, function (index, value) {
                toastr.error(value);
            });
        }
    });
});
// delete a post
$(document).on('click', '.delete-modal', function () {
    $('.modal-title').text('Delete');
    $('#id_delete').val($(this).data('id'));
    $('#factory_delete').val($(this).data('factory'));
    $('#deleteModal').modal('show');
    id = $('#id_delete').val();
});
$('.modal-footer').on('click', '.delete', function () {
    $.ajax({
        type: 'DELETE',
        url: 'posts/' + id,
        success: function (data) {
            toastr.success('Successfully deleted Post!', 'Success Alert', {timeOut: 5000});
            $('.item' + data['id']).remove();
        }
    });
});
