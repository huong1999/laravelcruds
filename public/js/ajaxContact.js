$(document).on('click', '.add-modal', function () {
    $('#infor_add').val('');
    $('#addModal').modal('show');
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('.modal-footer').on('click', '.add', function () {
    $.ajax({
        type: 'POST',
        url: 'contacts',
        data: {
            'infor': $('#infor_add').val(),
        },
        success: function (data) {
            console.log(data);
            toastr.success('Successfully added Contact!', 'Success Alert', {timeOut: 5000});
            $('#postTable').append("<tr class='item" + data.id + "'>"
                + "<td>" + data.id + "</td>"
                + "<td>" + data.infor + "</td>"
                + "<td>Right now</td>"
                + "<td>"
                + "<button class='edit-modal btn btn-info' data-id='" + data.id + "' data-infor='" + data.infor + "'><span class='glyphicon glyphicon-edit'></span> Edit</button>"
                + "<button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-infor='" + data.infor + "'><span class='glyphicon glyphicon-trash'></span> Delete</button>"
                + "</td>"
                + "</tr>");
        },
        error: function (data) {
            res = $.parseJSON(data.responseText);
            $.each(res.errors, function (index, value) {
                toastr.error(value);
            });
        }
    });
});

// Edit a contact
$(document).on('click', '.edit-modal', function () {
    $('.modal-title').text('Edit');
    $('#id_edit').val($(this).data('id'));
    $('#infor_edit').val($(this).data('infor'));
    id = $(this).data('id');
    $('#editModal').modal('show');
});
$('.modal-footer').on('click', '.edit', function () {
    $.ajax({
        type: 'PUT',
        url: 'contacts/' + id,
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $("#id_edit").val(),
            'infor': $('#infor_edit').val(),
        },
        success: function (data) {
            toastr.success('Successfully updated Contact!', 'Success Alert', {timeOut: 5000});
            $('.item' + data.id).replaceWith(
                "<tr class='item" + data.id + "'>"
                + "<td>" + data.id + "</td>"
                + "<td>" + data.infor + "</td>"
                + "<td>Right now</td>"
                + "<td>"
                + "<button class='edit-modal btn btn-info' data-id='" + data.id + "' data-infor='" + data.infor + "'><span class='glyphicon glyphicon-edit'></span> Edit</button>"
                + "<button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-infor='" + data.infor + "'><span class='glyphicon glyphicon-trash'></span> Delete</button>"
                + "</td>"
                + "</tr>"
            );
        },
        error: function (data) {
            res = $.parseJSON(data.responseText);
            $.each(res.errors, function (index, value) {
                toastr.error(value);
            });
        }
    });
});
// delete a contact
$(document).on('click', '.delete-modal', function () {
    $('.modal-title').text('Delete');
    $('#id_delete').val($(this).data('id'));
    $('#infor_delete').val($(this).data('infor'));
    $('#deleteModal').modal('show');
    id = $('#id_delete').val();
});
$('.modal-footer').on('click', '.delete', function () {
    $.ajax({
        type: 'DELETE',
        url: 'contacts/' + id,
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {
            toastr.success('Successfully deleted Contact!', 'Success Alert', {timeOut: 5000});
            $('.item' + data['id']).remove();
        },
        error: function (data) {
            res = $.parseJSON(data.responseText);
            $.each(res.errors, function (index, value) {
                toastr.error(value);
            });
        }
    });
});
