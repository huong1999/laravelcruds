<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\PostRequest;
use App\Post;
use Illuminate\Http\Request;
use Response;
use Validator;
use View;

class PostsController extends Controller
{
    protected $post;

    function __construct(Post $post)
    {
        $this->middleware('auth');  //switch login page when not logged in
        $this->post = $post;
    }

    public function index()
    {
        $contacts = Contact::all();
        $posts = $this->post->showPost();

        return view('modal_crud.post.list', ['contacts' => $contacts, 'posts' => $posts]);
    }

    public function store(PostRequest $request)
    {
        $posts = $this->post->addPost($request);
        $contacts = $posts->contacts->infor;

        return response()->json(['posts' => $posts, 'contacts' => $contacts]);
    }

    public function update(PostRequest $request, $id)
    {
        $posts = $this->post->updatePost($request, $id);
        $contacts = $posts->contacts->infor;

        return response()->json(['posts' => $posts, 'contacts' => $contacts]);

    }

    public function destroy($id)
    {
        $posts = $this->post->deletePost($id);

        return response()->json($posts);
    }


    public function show(Request $request)
    {
        $key = $request->keyword;
        $posts = $this->post->searchPost($key);
        $contacts = $posts->contacts->infor;

        return view('modal_crud.post.list', ['key' => $key, 'posts' => $posts, 'contacts' => $contacts]);
    }

}