<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $table = 'contacts';
    protected $fillable = ['id', 'infor'];

    public function post()
    {
        return $this->hasMany('App\Post', 'contact_id', 'id');
    }

    public function showContact()
    {
        $contacts = Contact::all();
        return $contacts;
    }

    public function addContact($input)
    {
        $contact = new Contact();
        $contact->infor = $input->infor;
        $contact->save();
        return $contact;
    }

    public function updateContact($input, $id)
    {
        $contact = Contact::findOrFail($id);
        $contact->infor = $input->infor;
        $contact->save();

        return $contact;
    }

    public function deleteContact($id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();

        return $contact;
    }

    public function searchContact($key)
    {
        //Query Builder
        $contacts = Contact::where('contacts.infor', 'like', "%$key%")->get();
        return $contacts;
    }

    public function showListPosts($id)
    {
        $post = Post::select('posts.*')
            ->join('contacts', 'posts.contact_id', '=', 'contacts.id')
            ->where('posts.contact_id', '=', $id)
            ->get();
        return $post;
    }

}
