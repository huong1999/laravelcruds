<!-- Modal form to add a post -->
<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-factory"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="factory">Factory:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="factory_add" autofocus>
                            <small>Min: 2, Max: 32, only text</small>
                            <p class="text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="contact">Job:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="job_add" autofocus>
                            <small>Min: 1, Max: 32, only text</small>
                            <p class="text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="contact_id">Contact:</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="contact_add" name="contact_id">
                                @foreach($contacts as $contact)
                                    <option value="{{$contact->id}}">
                                        {{$contact->id}}-{{$contact->infor}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="describe">Describe:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="describe_add" cols="40" rows="5"></textarea>
                            <small>Min: 2, Max: 128, only text</small>
                            <p class="text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success add" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-check'></span> Add
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>