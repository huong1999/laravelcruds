<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = ['id', 'job', 'factory', 'contact', 'describe', 'contact_id'];

    public function contacts()
    {
        return $this->belongsTo('App\Contact', 'contact_id', 'id');
    }

    public function showPost()
    {
        $posts = Post::with('contacts')->get();

        return $posts;
    }

    public function addPost($input)
    {
        $post = new Post();
        $post->job = $input->job;
        $post->factory = $input->factory;
        $post->describe = $input->describe;
        $post->contact_id = $input->contact_id;
        $post->save();

        return $post;
    }

    public function updatePost($input, $id)
    {
        $post = Post::findOrFail($id);      //output the first result of query
        $post->job = $input->job;
        $post->factory = $input->factory;
        $post->describe = $input->describe;
        $post->contact_id = $input->contact_id;
        $post->save();

        return $post;

    }

    public function deletePost($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return $post;
    }

    public function searchPost($key)
    {
        $posts = Post::select('posts.*')
            ->join('contacts', 'posts.contact_id', '=', 'contacts.id')
            ->where('contacts.infor', 'like', "%$key%")
            ->orwhere('posts.factory', 'like', "%$key%")
            ->orwhere('posts.job', 'like', "%$key%")
            ->orwhere('posts.describe', 'like', "%$key%")
            ->orwhere('posts.contact_id', 'like', "%$key%")
            ->get();    //take data

        return $posts;

    }
}