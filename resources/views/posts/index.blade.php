<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">

    <!-- CSFR token for ajax call -->
    <meta name="_token" content="{{ csrf_token() }}"/>

    <title>Thông tin tuyển dụng</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- icheck checkboxes -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.css">

    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.2.4.js"
            integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <!-- Bootstrap JavaScript -->
    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

    <!-- toastr notifications -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- icheck checkboxes -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

    <style>
        .panel-heading {
            padding: 0;
        }

        .panel-heading ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        .panel-heading li {
            float: left;
            border-right: 1px solid #bbb;
            display: block;
            padding: 14px 16px;
            text-align: center;
        }

        .panel-heading li:last-child:hover {
            background-color: #ccc;
        }

        .panel-heading li:last-child {
            border-right: none;
        }

        .panel-heading li a:hover {
            text-decoration: none;
        }

        .table.table-bordered tbody td {
            vertical-align: baseline;
        }

        .search-navbar {
            margin-left: 700px;
        }

        .login {
            margin-left: 1100px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            color: #fff;
            background-color: #428bca;
            font-weight: bold;
            font-size: 16px;
        }

        .margintop20 {
            margin-top: -80px;
            width: 300px;
        }

        .nav-pills > li > a {
            border-radius: 0px;
        }

        a {
            color: #000;
            text-decoration: none;
        }

        a:hover {
            color: #000;
            text-decoration: none;
        }

        .nav-stacked > li + li {
            margin-top: 0px;
            margin-left: 0;
            border-bottom: 1px solid #dadada;
            border-left: 1px solid #dadada;
            border-right: 1px solid #dadada;
        }

        .active2 {
            border-right: 4px solid #428bca;
        }
    </style>

</head>

<body>
</br>
<div class="title-page">
    <h2 class="text-center">Admin tuyển dụng IT</h2>
</div>
<div class="row">
    <div class="col-md-4 column margintop20">

        <ul class="nav nav-pills nav-stacked" style="background-color: #f8f9fa">
            <li class="active text-center"><a href="#">Menu</a>
            </li>
            <li><a href="http://localhost:8088/posts" class="active1"><span
                            class="glyphicon glyphicon-chevron-right"></span>Post</a>
            </li>
            <li><a href="http://localhost:8088/contacts" class="active2"><span
                            class="glyphicon glyphicon-chevron-right"></span>Contact</a>
            </li>
            <li><a href="http://localhost:8088/logout" class="active3"><span
                            class="glyphicon glyphicon-chevron-right"></span>Logout</a>
            </li>
            <li style="height:1500px">
            </li>
        </ul>
    </div>
    @yield('content')
</div>
</body>
</html>