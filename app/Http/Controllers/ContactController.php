<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    protected $contact;

    function __construct(Contact $contact)
    {
        $this->middleware('auth');
        $this->contact = $contact;
    }

    public function index()
    {
        $contacts = $this->contact->showContact();
        return view('modal_crud.contact.list', ['contacts' => $contacts]);
    }


    public function store(ContactRequest $request)
    {
        $contact = $this->contact->addContact($request);
        return response()->json($contact);
    }

    public function update(ContactRequest $request, $id)
    {
        $contact = $this->contact->updateContact($request, $id);
        return response()->json($contact);
    }

    public function destroy($id)
    {
        $contact = $this->contact->deleteContact($id);
        return response()->json($contact);
    }

    public function show(Request $request)
    {
        $key = $request->keyword;
        $contacts = $this->contact->searchContact($key);
        return view('modal_crud.contact.search', ['contacts' => $contacts, 'key' => $key]);
    }

    public function showPost($id)
    {
        $contacts = Contact::all();
        $posts = $this->contact->showListPosts($id);
        return view('modal_crud.contact.show', ['posts' => $posts, 'contacts' => $contacts]);
    }

}
