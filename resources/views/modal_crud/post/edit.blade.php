<!-- Modal form to edit a form -->
<div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-factory"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Factory">Factory:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="factory_edit" autofocus>
                            <small>Min: 2, Max: 32, only text</small>
                            <p class="text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="job">Job:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="job_edit" autofocus>
                            <small>Min: 1, Max: 32, only text</small>
                            <p class="text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="contact_id">Contact:</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="contact_edit">
                                @foreach($contacts as $contact)
                                    <option value="{{$contact->id}}" id="contact_id">
                                        {{$contact->id}}-{{$contact->infor}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="describe">Describe:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="describe_edit" cols="40" rows="5"></textarea>
                            <small>Min: 2, Max: 128, only text</small>
                            <p class="text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                            <span class='glyphicon glyphicon-check'></span> Edit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>