<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function getLogin()
    {
        return view('auth/login');
    }

    public function postLogin(LoginRequest $request)
    {

        $validator = Validator::make($request->all(), $request->rules(), $request->messages());

        if ($validator->fails()) {
            // Invalid data -> switch login and output error
            return redirect('login')->withErrors($validator)->withInput();
        } else {
            // If valid datatype
            $email = $request->input('email');
            $password = $request->input('password');
            //Check if the data entered is valid or not
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                // Login success
                return redirect('posts');
            } else {
                // Login fail -> notification error
                Session::flash('error', 'Invalid email or password!');
                return redirect('login');
            }
        }
    }
}
