<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//CRUD-Search Post
Route::resource('posts', 'PostsController');
Route::get('posts/contact/{id}', 'ContactController@showPost');

//CRUD Contact
Route::resource('contacts', 'ContactController');

// Home
Route::get('/', 'PostsController@index');
Route::get('/home', 'PostsController@index');

// Register
Route::get('register', 'Auth\RegisterController@getRegister');
Route::post('register', 'Auth\RegisterController@postRegister');

// Login
Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@getLogin']);
Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@postLogin']);

// Logout
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LogoutController@getLogout']);
