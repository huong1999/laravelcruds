@extends('posts.index')
@section('content')

    <div class="col-md-8 col-md-offset-0">

        <br/>
        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">

                <li>
                    <button class="add-modal btn btn-success">
                        <span class="glyphicon glyphicon-plus"></span>Add a Contact
                    </button>
                </li>
                <li class="search-navbar navbar navbar-light bg-light">
                    <form class="form-inline" action="/contacts/search" method="" role="search">
                        <input id="search" name="keyword" class="form-control mr-sm-2" type="text" placeholder="Search"
                               aria-label="Search">
                    </form>
                </li>
                </ul>
            </div>

            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover" id="postTable">
                    <thead>
                    <tr>
                        <th valign="middle">ID</th>
                        <th>Infor</th>
                        <th>Last updated</th>
                        <th>Actions</th>
                    </tr>
                    {{ csrf_field() }}
                    </thead>
                    <tbody>
                    @foreach($contacts as $contact)
                        <tr class="item{{$contact->id}}">
                            <td>{{$contact->id}}</td>
                            <td><a href="/posts/contact/{{$contact->id}}">{{$contact->infor}}</a></td>
                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $contact->updated_at)->diffForHumans() }}</td>
                            <td>
                                <button class="edit-modal btn btn-info mr-1"
                                        data-id="{{$contact->id}}"
                                        data-infor="{{$contact->infor}}"
                                <span class="glyphicon glyphicon-edit"></span> Edit
                                </button>
                                <button class="delete-modal btn btn-danger"
                                        data-id="{{$contact->id}}"
                                        data-infor="{{$contact->infor}}">
                                    <span class="glyphicon glyphicon-trash"></span> Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->
    @include('modal_crud.contact.add')
    @include('modal_crud.contact.edit')
    @include('modal_crud.contact.delete')

    <!-- Delay table load until everything else is loaded -->
    {{--    <script>--}}
    {{--        $(window).load(function () {--}}
    {{--            $('#postTable').removeAttr('style');--}}
    {{--        })--}}
    {{--    </script>--}}
    <script type="text/javascript" src="js/ajaxContact.js"></script>
@endsection