@extends('posts.index')
@section('content')

    <div class="col-md-8 col-md-offset-0">

        <br/>
        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">
                <li class="search-navbar navbar navbar-light bg-light">
                    <form class="form-inline" action="/posts/search" method="" role="search">
                        <input id="search" name="keyword" class="form-control mr-sm-2" type="text" placeholder="Search"
                               aria-label="Search">
                    </form>
                </li>
                </ul>
            </div>

            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover" id="postTable"
                       style="visibility: hidden;">
                    <thead>
                    <tr>
                        <th valign="middle">ID</th>
                        <th>Factory</th>
                        <th>Job</th>
                        <th>Describe</th>
                        <th>Contact</th>
                        <th>Last updated</th>
                    </tr>
                    {{ csrf_field() }}
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr class="item{{$post->id}}">
                            <td>{{$post->id}}</td>
                            <td>{{$post->factory}}</td>
                            <td>{{$post->job}}</td>
                            <td>{{$post->describe}}</td>
                            <td><a href="/posts/contact/{{$post->contact_id}}">{{$post->contacts['infor']}}</a></td>
                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->diffForHumans() }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->
    @include('modal_crud.post.add')
    @include('modal_crud.post.edit')
    @include('modal_crud.post.delete')

    <!-- Delay table load until everything else is loaded -->
    <script>
        $(window).load(function () {
            $('#postTable').removeAttr('style');
        })
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/ajaxPost.js') }}"</script>
@endsection