<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'factory' => 'required|min:2|max:32',
            'job' => 'required|min:1|max:32',
            'describe' => 'required|min:2|max:32',
        ];
    }

    public function messages()
    {
        return [
            'factory.required' => 'Please, enter factory of post....',
            'factory.min' => 'Factory: At least 2 characters...',
            'factory.max' => 'Factory: At most 32 characters...',
            'job.required' => 'Please, enter job of factory....',
            'job.min' => 'Job: At least 1 characters...',
            'job.max' => 'Job: At most 32 characters...',
            'describe.required' => 'Please, enter describe of post....',
            'describe.min' => 'Describe: At least 2 characters...',
            'describe.max' => 'Describe: At least 32 characters...',
        ];
    }

}
